package xyz.humcorp.sionapp.utils.core;

public interface IBaseView {
    void showProgress();

    void hideProgress();

    void showContainer();

    void hideContainer();

    void showError(String message);

    void showSuccess(String message);
}
