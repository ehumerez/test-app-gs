package xyz.humcorp.sionapp.utils.core;

public interface IBasePresenter {
    void onSuccess(String message);

    void onFailure(String message);
}
