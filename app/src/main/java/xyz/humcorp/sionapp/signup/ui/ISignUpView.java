package xyz.humcorp.sionapp.signup.ui;

import xyz.humcorp.sionapp.utils.core.IBaseView;
import xyz.humcorp.sionapp.entities.User;

public interface ISignUpView extends IBaseView {
    void providerUserProfile(User user);
}
