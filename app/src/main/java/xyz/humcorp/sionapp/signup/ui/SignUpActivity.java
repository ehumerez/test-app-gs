package xyz.humcorp.sionapp.signup.ui;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import xyz.humcorp.sionapp.main.MainActivity;
import xyz.humcorp.sionapp.R;
import xyz.humcorp.sionapp.signup.ISignUpPresenter;
import xyz.humcorp.sionapp.signup.SignUpPresenter;
import xyz.humcorp.sionapp.utils.Utils;
import xyz.humcorp.sionapp.entities.User;

public class SignUpActivity extends AppCompatActivity implements ISignUpView {

    @BindView(R.id.activity_signup_name)
    TextInputEditText activity_signup_name;
    @BindView(R.id.activity_signup_lastname)
    TextInputEditText activity_signup_lastname;
    @BindView(R.id.activity_signup_phone)
    TextInputEditText activity_signup_phone;
    @BindView(R.id.activity_signup_email)
    TextInputEditText activity_signup_email;
    @BindView(R.id.activity_signup_password)
    TextInputEditText activity_signup_password;
    @BindView(R.id.activity_signup_password2)
    TextInputEditText activity_signup_password2;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.container)
    ConstraintLayout container;
    @BindView(R.id.activity_signup_btn)
    Button activity_signup_btn;
    @BindView(R.id.textInputLayoutEmail)
    TextInputLayout textInputLayoutEmail;
    @BindView(R.id.textInputLayoutPass)
    TextInputLayout textInputLayoutPass;
    @BindView(R.id.textInputLayoutPass2)
    TextInputLayout textInputLayoutPass2;

    private ISignUpPresenter mPresenter;
    private boolean isEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupView();
        setupVars();
    }

    private void setupView() {
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
    }

    private void setupVars() {
        mPresenter = new SignUpPresenter(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        isEdit = getIntent().getBooleanExtra("PROFILE", false);
        if (isEdit)
            mPresenter.getUserProfile();
        else
            setupSignupView();
    }

    @Override
    public void providerUserProfile(User user) {
        setupProfileView(user);
    }

    private void setupSignupView() {
        textInputLayoutEmail.setVisibility(View.VISIBLE);
        textInputLayoutPass.setVisibility(View.VISIBLE);
        textInputLayoutPass2.setVisibility(View.VISIBLE);
        getSupportActionBar().setTitle("Registro");
        activity_signup_btn.setText("Registrar");
    }

    private void setupProfileView(User user) {
        hideViews();
        activity_signup_name.setText(user.getName());
        activity_signup_lastname.setText(user.getLastname());
        activity_signup_phone.setText(user.getPhone());
    }

    private void hideViews() {
        textInputLayoutEmail.setVisibility(View.GONE);
        textInputLayoutPass.setVisibility(View.GONE);
        textInputLayoutPass2.setVisibility(View.GONE);
        getSupportActionBar().setTitle("Mi perfil");
        activity_signup_btn.setText("Actualizar");
    }

    @OnClick(R.id.activity_signup_btn)
    public void handleRegister() {
        String name = activity_signup_name.getText().toString().trim();
        String lastName = activity_signup_lastname.getText().toString().trim();
        String phone = activity_signup_phone.getText().toString().trim();
        String email = activity_signup_email.getText().toString().trim();
        String password = activity_signup_password.getText().toString().trim();
        String password2 = activity_signup_password2.getText().toString().trim();
        if (name.isEmpty()) {
            Utils.showMessage(this, "Ingresar nombre");
            return;
        }
        if (lastName.isEmpty()) {
            Utils.showMessage(this, "Ingresar apellidos ");
            return;
        }
        if (!isEdit && email.isEmpty()) {
            Utils.showMessage(this, "Ingresar un correo electrónico");
            return;
        }
        if (!isEdit && password.isEmpty()) {
            Utils.showMessage(this, "Ingresar contraseña");
            return;
        }
        if (!isEdit && password2.isEmpty()) {
            Utils.showMessage(this, "Ingresar nuevamente la contraseña");
            return;
        }

        if (!password.equals(password2)) {
            Utils.showMessage(this, "Las contraseñas deben ser las mismas");
            return;
        }
        mPresenter.handleRegister(isEdit, name, lastName,
                phone, email, password);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showContainer() {
        container.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideContainer() {
        container.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        if (isEdit)
            hideViews();
        Utils.showMessage(this, message);
    }

    @Override
    public void showSuccess(String message) {
        Utils.showMessage(this, message);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }
}