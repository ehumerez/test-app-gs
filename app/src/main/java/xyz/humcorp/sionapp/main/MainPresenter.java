package xyz.humcorp.sionapp.main;

import android.content.Context;

import java.util.List;

import xyz.humcorp.sionapp.entities.Project;

public class MainPresenter implements IMainPresenter {

    private IMainView mView;
    private MainRepository mRepository;

    public MainPresenter(Context context, IMainView view) {
        mView = view;
        mRepository = new MainRepository(context, this);
    }

    @Override
    public void getProjects() {
        if (mView != null)
            mView.showProgress();
        mRepository.getProjects();
    }

    @Override
    public void handleNewProject(String proyectId, Object image, String projectName, String projectDescription) {
        if (mView != null) {
            mView.showProgress();
        }
        mRepository.handleNewProject(proyectId, image, projectName, projectDescription);
    }

    @Override
    public void handleDeleteProject(String projectId) {
        if (mView != null) {
            mView.showProgress();
        }
        mRepository.deleteProject(projectId);
    }

    @Override
    public void onSuccess(String message) {
        if (mView != null) {
            mView.hideProgress();
            mView.showSuccess(message);
        }
    }

    @Override
    public void onFailure(String message) {
        if (mView != null) {
            mView.hideProgress();
            mView.showError(message);
        }
    }

    @Override
    public void onSuccess(List<Project> projects) {
        if (mView != null) {
            mView.hideProgress();
            mView.providerProjects(projects);
        }
    }

    @Override
    public void onSuccessDelete(String message) {
        if (mView != null) {
            mView.hideProgress();
            mView.onSuccessDelete(message);
        }
    }

    @Override
    public void showMessage(String message) {
        if (mView != null)
            mView.showProgressUploadImage(message);
    }
}
