package xyz.humcorp.sionapp.main;

import java.util.List;

import xyz.humcorp.sionapp.utils.core.IBasePresenter;
import xyz.humcorp.sionapp.entities.Project;

public interface IMainPresenter extends IBasePresenter {
    void handleNewProject(String proyectId, Object image, String projectName, String projectDescription);

    void getProjects();

    void onSuccess(List<Project> projects);

    void handleDeleteProject(String projectId);

    void onSuccessDelete(String message);

    void showMessage(String message);
}
