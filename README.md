Bienvenidos a mi aplicación de prueba para la empresa Grupo Sion.
Objetivo:  Sion App Test es una aplicación que tiene como objetivo gestionar información básica de proyectos de una empresa pertenecientes a un usuario.
Descripción: Aplicación Android nativa implementado con el lenguaje de programación Java, el cual tiene las siguientes funcionalidades:
1) Registro de usuario
    - Nombres
    - Apellidos
    - Teléfono (Opcional)
    - Correo electrónico
    - Contraseña
    - Repetir contraseña
2) Inicio de sesión
    - Correo electrónico
    - Contraseña
3) Crear nuevo proyecto
    - Foto del proyecto (Administrar permisos de cámara y almacenamiento en sdk >= 23)
    - Nombre del proyecto
    - Descripción
4) Editar proyecto
    - Foto del proyecto
    - Nombre del proyecto
    - Descripción
5) Eliminar proyecto
6) Listar proyectos
7) Mostrar mi perfil
    - Nombres
    - Apellidos
    - Teléfono
8) Actualizar mi perfil
    - Nombres
    - Apellidos
    - Teléfono
9) Buscador de proyectos por su nombre o descripción
10) Deslizar para actualizar listado de proyectos (SwipeRefreshLayout)

APLICACIÓN MÓVIL
    Configuraciones:
    - Versión de Gradle 4.0.1
    - targetSdkVersion 29
    - minSdkVersion 21
    Pasos para compilar y ejecutar:
    1.- Clonar el proyecto y abrir el folder en Android Studio
    2.- Ejecutar en emulador o dispositivo físico
BACKEND
    - Autenticación con proveedor de acceso por email y password de Firebase
    - Base de datos Cloud Firestore de Firebase
    - Storage de Firebase para almacenar las imágenes de cada proyecto

    Enviaré una invitación a Rosember y Raúl a sus correos electrónicos para agregarlos en firebase para que puedan ver el pequeño backend del proyecto.